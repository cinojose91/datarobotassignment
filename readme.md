# MongoDB Cluster ( Data Robot Assignment)

This repo contain code required to create the mongo DB cluster in AWS and ansible playbook to prepare and deploy a mongodb container.

## Getting Started

Clone the github url to your local using the following git command.

```
git clone https://cinojose91@bitbucket.org/cinojose91/datarobotassignment.git
```
### AWS Credentials setup
Update your aws credential file with the api key and access key of the aws account. You can find the aws credential file in the following location
```bash
vi ~/.aws/credentials
```
add the following to the file, here profile name is **dev-account**

```
[dev-account]
aws_access_key_id = AKXXXXXXXXXXXXXXXXXX
aws_secret_access_key = JttYXXXXXXXXXXXXXXXXXXXXXXXXXX
region = us-east-1
output = json
```



### Prerequisites

For this code to work you may need to install the following tools:

* [Ansible](http://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) ( Version 2.5.2)
* [Terraform](https://www.terraform.io/intro/getting-started/install.html) (Version 0.11.3)
  * AWS ( provider.aws: version = "~> 1.17")


## How to use?

### Terraform Component

Once the Repo is cloned on to the local. Browse to following location.


```bash
cd /datarobotassignment/terraform/provider/aws/mongo-cluster
```

update the variables based on the requirements, we uses the following variables.

`aws_region` Specify the AWS region where you want to run the states.eg : "us-east-1"

`aws_profile` Specify the profile to use, so terraform with use the credentials under that profile eg : "dev-account"

`cluster_count` Specify the size of the cluster, eg :"3" 

`mogovpc_id` Specify the VPC id where u want to run this states, make sure the specify the vpc in the above region eg : "vpc-aa623cd1"

`availability_zone` Specify the avaialbility zones for the cluster creation, just specify the 3 zones needed eg : ["us-east-1a","us-east-1a","us-east-1c"]

`data_volume_size` Specify the size of the datavolume in Gb, this will be used for creating the EBS volumes eg : "1000"

`subnet_id` Specify the subnets where the cluster need to be created, specify three since the size of the cluster is 3 eg : ["subnet-123","subnet-456","subnet-789"]

`mongo_ami`  Specify the ami id to be used for the instance creation, eg :"ami-0987654"

`instance_type` Specify the instance type on which the cluster nodes should be created recommended is to use "m4.large" 

`key_name` Specify the keypair to be used to connect to the instance eg : "mongo-cluster-keypair"

`dns_hosted_zone` Specify the dns hosted zone to be used for creating the dns records  eg: "test.com."

#### How to run?

After specifying the variables names, run the following to initialize terraform

```bash
terraform init
```

Once initialized, run the following to see what terraform will do , just a ***dry run***

```bash
terraform plan -var-file="./environment.tfvars"
```
The above command will tell you on how many resource will be created/removed etc, once you are happy with the result do the following to execute the state.

```bash
terraform apply -var-file="./environment.tfvars"
```

The above code will execute and you will see the cluster created in AWS

#### Repo Structure

```bash
|--terraform 
        |--modules  -->Contains all the modules that can be reused 
        |    |--aws  --> Subfolder for provider (AWS,GCE etc..)
        |        |--mongo-cluster  -->Module name
        |             |--main.tf   -->contains all the terraform states
        |             |--variables.tf -->contains variables required
        |             |--output.tf    -->contains the output values of the module, which can be used by other modukes
        |
        |--provider -->Contains the actual driver for the states, all the states are initated from here
             |--aws -->Subfolder for the provider ( AWS,GCE etc..)
                 |--mongo-cluster -->foldername for the states
                       |--main.tf  -->Contains the actual state file details, module call etc..
                       |--aws-provider.tf -->Contains provider specific settings
                       |--variables.tf -->Contains the varible declarions
                       |--environment.tfvars -->Contains the variable values

```

[More reading](https://www.terraform.io/docs/index.html)



### Ansible Component

Once you have completed the cluster creation and you can now start with deploying the contianer to the cluster.

```bash
cd datarobotassignment/ansible
```
The following file need to update with the details of the host that need to be connected and configured.

```bash
vi hosts
```
This update the host ip under the ***[mongo-host]*** , please follow the convention

```bash
<<IP of the host>> ansible_ssh_private_key_file=<<define the key file location>>
```


#### Variables

Following variables need to be updated 

```bash
cd datarobotassignment/ansible/docker.mongo/defaults/
vi main.yml

```

`containername` Specify the name of the container you  want to create

`continerimage` Specify the container image, you can specify the version after the ***:***  ,eg : mongo:3.4

#### How to run?

Once the hosts file is updated, please use the following to run the playbook

```bash
cd datarobotassignment/ansible

sudo ansible-playbook site.yml -i hosts

```

it will show the following output for the successfull run

```bash
PLAY RECAP *************************************************************************************************************************************************************************************************
13.229.116.85              : ok=10   changed=0    unreachable=0    failed=0
```

#### Repo Structure
```bash
|--ansible
    |
    |--hosts
    |--site.yml
    |--roles
        |
        |--docker -->Role for installing docker
        |   |
        |   |--defaults
        |   |    |--main.yml -->Contains the variables for this role
        |   |--tasks
        |       |--main.yml -->Contains the main ansbile states for installing docker
        |
        |--docker.mongo -->Role for deploying docker mongo contianer.
            |
            |--defaults
            |    |--main.yml -->Contains the variable for this role
            |--tasks
                 |--main.yml --> Contains the main ansible states of deploying container

```

## Reference
[Docker](http://docker.com/)

[Ansible](http://docs.ansible.com/)

[Terraform](https://www.terraform.io/docs/index.html)


