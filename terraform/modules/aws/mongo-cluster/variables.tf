#############################################################
#
# Define all variables required for the modules in this file
#
##############################################################

# Define the number of nodes in the cluster
variable "cluster_count" {}

# Variables for the Security group
variable "mogovpc_id" {}


# Variables required for the data disk
variable "availability_zone" {
  description = "Run the EC2 Instances in these Availability Zones"
  type        = "list"
  default     = ["us-east-1a", "us-east-1b", "us-east-1c"]
}
variable "data_volume_size" {}

# Variables for the instance
variable "subnet_id" {
  description = "Run the EC2 Instances in these subnets"
  type        = "list"
  default     = ["subnet-123", "subnet-456", "subnet-789"]
}
variable "mongo_ami" {}
variable "instance_type" {}
variable "key_name" {}

# Variables required for the DNS record creations
variable "dns_hosted_zone" {}
