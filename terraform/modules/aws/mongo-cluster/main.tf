
# Grabbbibng the VPC details
data "aws_vpc" "mongo-db-vpc" {
  id = "${var.mogovpc_id}"
}

# Security group for the mongo Instances
resource "aws_security_group" "mongo-instance-sg" {
  name        = "mongo-cluste-sg"
  description = "Allow cluster to communicate with each other"
  vpc_id      = "${var.mogovpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0
    to_port     = 27017
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.mongo-db-vpc.cidr_block}"]
  }

  ingress {
    from_port   = 0
    to_port     = 27018
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.mongo-db-vpc.cidr_block}"]
  }

  ingress {
    from_port   = 0
    to_port     = 27019
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.mongo-db-vpc.cidr_block}"]
  }
}

# # Security group for the mongo Instances
resource "aws_security_group" "allow-ssh-sg" {
  name        = "allow-ssh-access"
  description = "Allow ssh access to the nodes"
  vpc_id      = "${var.mogovpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.mongo-db-vpc.cidr_block}"]
  }
}

# Data volume for the node
resource "aws_ebs_volume" "data_volume" {
  availability_zone = "${element(var.availability_zone, count.index)}"
  size              = "${var.data_volume_size}"
  count             = "${var.cluster_count}"

  tags {
    Name = "drobot-cino-dev-mongo-data-volume-${count.index}"
  }
}

# Create the instances based on the specified count
resource "aws_instance" "mongonode" {
  ami                    = "${var.mongo_ami}"
  instance_type          = "${var.instance_type}"
  availability_zone      = "${element(var.availability_zone, count.index)}"
  subnet_id              = "${element(var.subnet_id, count.index)}"
  key_name               = "${var.key_name}"
  count                  = "${var.cluster_count}"
  vpc_security_group_ids = ["${aws_security_group.mongo-instance-sg.id}", "${aws_security_group.allow-ssh-sg.id}"]

  tags {
    Name = "drobot-cino-dev-mongo-aha-${count.index}"
  }
}

# Attach data volume to the instance
resource "aws_volume_attachment" "mongo-data-volume-attachment" {
  count       = "${var.cluster_count}"
  device_name = "/dev/sdh"
  volume_id   = "${element(aws_ebs_volume.data_volume.*.id, count.index)}"
  instance_id = "${element(aws_instance.mongonode.*.id, count.index)}"
}

# Find the zone id
data "aws_route53_zone" "drobot-dns" {
  name         = "${var.dns_hosted_zone}"
  private_zone = false
}

# Adding the DNS record to the zone
resource "aws_route53_record" "sym-hadoop-dev-shared-emea-r53" {
  count   = "${var.cluster_count}"
  zone_id = "${data.aws_route53_zone.drobot-dns.zone_id}"
  name    = "drobot-mongo-ha-${count.index}.test.com"
  type    = "A"
  ttl     = "300"
  records = ["${element(aws_instance.mongonode.*.private_ip, count.index)}"]
}
