# Default variable for cluster creation
aws_region       = "us-east-1"
aws_profile      = "cino-dev-account"


# Mongo cluster module variables
cluster_count     = "3"
mogovpc_id        = "vpc-aa623cd1"
availability_zone = ["us-east-1a","us-east-1a","us-east-1c"]
data_volume_size  = "1000"
subnet_id         = ["subnet-123","subnet-456","subnet-789"]
mongo_ami         = "ami-0987654"
instance_type     = "m4.large"
key_name          = "mongo-cluster-keypair"
dns_hosted_zone   = "test.com."