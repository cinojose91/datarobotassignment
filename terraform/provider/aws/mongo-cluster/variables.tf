variable "aws_region" {}
variable "aws_profile" {}

# Variables required for the mogodbcluster module

variable "cluster_count" {}
variable "mogovpc_id" {}

variable "availability_zone" {
  description = "Run the EC2 Instances in these Availability Zones"
  type        = "list"
  default     = ["us-east-1a", "us-east-1b", "us-east-1c"]
}
variable "data_volume_size" {}
variable "subnet_id" {
  description = "Run the EC2 Instances in these subnets"
  type        = "list"
  default     = ["subnet-123", "subnet-456", "subnet-789"]
}
variable "mongo_ami" {}
variable "instance_type" {}
variable "key_name" {}
variable "dns_hosted_zone" {}
