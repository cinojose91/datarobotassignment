#  Module to build the mongo cluster based on the following requirements.
#  Please provide the variable details in the environment.tfvars file
module "mongo-cluster-deployment" {
  source            = "../../../modules/aws/mongo-cluster/"
  cluster_count     = "${var.cluster_count}"
  mogovpc_id        = "${var.mogovpc_id}"
  availability_zone = "${var.availability_zone}"
  data_volume_size  = "${var.data_volume_size}"
  subnet_id         = "${var.subnet_id}"
  mongo_ami         = "${var.mongo_ami}"
  instance_type     = "${var.instance_type}"
  key_name          = "${var.key_name}"
  dns_hosted_zone   = "${var.dns_hosted_zone}"
}
